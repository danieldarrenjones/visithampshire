<nav class="bg-blue py-6">
    <div class="container mx-auto flex items-center justify-between flex-wrap px-2">
        <div class="flex items-center flex-no-shrink text-white mr-6">
            <a href="/" class="font-semibold text-xl tracking-tight text-white hover:text-blue-lighter no-underline">
                Visit Hampshire
            </a>
        </div>
        <div class="block lg:hidden">
            <button class="flex items-center px-3 py-2 border rounded text-blue-lighter border-blue-light hover:text-white hover:border-white">
                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
            </button>
        </div>
        <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">

            <div class="text-sm lg:flex-grow">
                {{-- <a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4">
                    Docs
                </a> --}}
                {{-- <a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4">
                    Examples
                </a> --}}
                {{-- <a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white">
                    Blog
                </a> --}}
            </div>

            <div>
                {{-- @guest
                    <a class="no-underline hover:underline text-white pr-3 text-sm" href="{{ url('/login') }}">Login</a>
                    <a class="no-underline hover:underline text-white text-sm" href="{{ url('/register') }}">Register</a>
                @else
                    <span class="text-white text-sm pr-4">{{ Auth::user()->name }}</span>

                    <a href="{{ route('logout') }}"
                        class="no-underline hover:underline text-white text-sm"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endguest --}}
            </div>

        </div>
    </div>
</nav>
