<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationBooking extends Model
{
    public $table = 'accommodation_bookings';

    public $timestamps = false;

    public $guarded = [
        'id'
    ];

    public function accommodation() {
        return $this->belongsTo('App\Accommodation', 'accommodation_id');
    }

    public function date() {
        return $this->belongsTo('App\AccommodationDate', 'accommodation_date_id');
    }
}
